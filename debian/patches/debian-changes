The Debian packaging of sesman is maintained in git, using the merging
workflow described in dgit-maint-merge(7). There isn't a patch queue
that can be represented as a quilt series.

A detailed breakdown of the changes is available from their canonical
representation - git commits in the packaging repository. For example,
to see the changes made by the Debian maintainer in the first upload
of upstream version 1.2.3, you could use:

    % git clone https://git.dgit.debian.org/sesman
    % cd sesman
    % git log --oneline 1.2.3..debian/1.2.3-1 -- . ':!debian'

(If you have dgit, use `dgit clone sesman`, rather than plain `git
clone`.)

A single combined diff, containing all the changes, follows.
--- sesman-0.3.4.orig/sesman.el
+++ sesman-0.3.4/sesman.el
@@ -4,7 +4,7 @@
 ;; Author: Vitalie Spinu
 ;; URL: https://github.com/vspinu/sesman
 ;; Keywords: process
-;; Version: 0.3.3-DEV
+;; Version: 0.3.4
 ;; Package-Requires: ((emacs "25"))
 ;; Keywords: processes, tools, vc
 ;;
